(ns myapp.core
  (:gen-class))


;;1
(def v2016 [-5 -6 -1 0 6 10 12 16 20 32 11 7])
(def v2017 [-3 -2 5 6 6 12 14 15 21 25 13 7])
(def kaikki (concat v2016 v2017))
(def positiiviset(filter #(> % 0) kaikki))
(defn keskiarvo
  [arvot]
  (float
  (/ (reduce + arvot) (count arvot))))

;;2
(def food-journal
  [{:kk 3 :paiva 1 :neste 5.3 :vesi 2.0}
   {:kk 3 :paiva 2 :neste 5.1 :vesi 3.0}
   {:kk 3 :paiva 13 :neste 4.9 :vesi 2.0}
   {:kk 4 :paiva 5 :neste 5.0 :vesi 2.0}
   {:kk 4 :paiva 10 :neste 4.2 :vesi 2.5}
   {:kk 4 :paiva 15 :neste 4.0 :vesi 2.8}
   {:kk 4 :paiva 29 :neste 3.7 :vesi 2.0}
   {:kk 4 :paiva 30 :neste 3.7 :vesi 1.0}])

(reduce +  (map
             #(- (% :neste) (% :vesi))
             (filter #(= (% :kk) 4) food-journal)))

;;3
 (def food-journal2
   (vec (map
          #(merge (select-keys % [:kk :paiva]) {:muuneste (format "%.2f" (- (% :neste) (% :vesi)))})
          (filter #(= (% :kk) 4) food-journal))))


(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println (keskiarvo positiiviset)))

