(ns test-lein.core
  (:gen-class))

(defn square [x] (* x x))

(defn karkausvuosi
  [year]
  (cond (zero? (mod year 400)) true
        (zero? (mod year 100)) false
        (zero? (mod year 4)) true
        :default false))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!"))




;; t.2 (+ 4 (* 2 5))
;; (+ 1 2 3 4 5)
;;((fn [who] (str "Tervetuloa tylypahkaan, " who "!")) "Arttu" )
;; (:middle (:name {:name {:first "Urho" :middle "Kaleva" :last "Kekkonen"}}))