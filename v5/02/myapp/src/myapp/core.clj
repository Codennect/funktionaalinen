(ns myapp.core
  (:gen-class))


(defn parillinen [] 
    (println "Give a number")
    (let [luku (try (Integer. (read-line)) (catch NumberFormatException e nil))]
      (if (and luku (or (> luku 0) (not (= luku 0 ))))
          (if (= (mod luku 2) 0)
            (println "number is even")
            (println "number is not even"))
          (do (println "error in input") (recur))
          )))


(defn jaolliset [maksimi]
  (loop [luku 1]
        (when (< luku maksimi)
          (do
          (when (= (mod luku 3) 0)
                (println luku))
              (recur (inc luku)))
        )))

     
(defn lotto []
  (def numerot (shuffle (take 40 (range))))
       (loop [i 0 lista []]
         (if (< i 7)
            (recur (inc i) (conj lista (+ (nth numerot i) 1)))
            (println lista))))
          

(defn syt [a b]
        (if (zero? b)
          (println a)
          (recur b (mod a b))))


(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (syt 1023 858)
)
