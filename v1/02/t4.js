var potenssi = function(base, exponent) {
  return exponent == 0? 1 : base * potenssi(base, --exponent);
};

//console.log(potenssi(2,10))

function uusiPotenssi(base, exponent) {
    return potenssiHelper(base, exponent);
// Käytetään apufunktiota
    }
function potenssiHelper(base, exponent) {
    if (exponent === 1) {
        return base;
    }else {
        return base* potenssiHelper(base, --exponent);
    }
}
console.log(uusiPotenssi(2,3));