/*Keskenään jaottomiksi tai suhteellisiksi alkuluvuiksi tai alkuluvuiksi toistensa 
suhteen sanotaan kahta lukua p ja q, jos p:n ja q:n suurin yhteinen tekijä on 
1. Sovella edellisen tehtävän algoritmia ja tee rekursiivinen funktio kjl(p, q), 
joka tutkii ovatko kaksi lukua keskenään jaottomia. Esimerkiksi 35 ja 18 ovat keskenään jaottomia lukuja.*/


var kjl = function(p,q) {
      if(q===1) {
        console.log("Luvut eivät ole jaollisia")
        return false
      }
      else if(q===0) {
          return true
      }
      return kjl(q, p % q);
        
};

console.log(kjl(11,2))