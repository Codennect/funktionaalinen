package t1;

import java.util.function.IntSupplier;
import java.util.function.ToIntFunction;

class Functional2 implements IntSupplier{
    
    // Hyödynnetään itse määriteltyä GeneratorFIF-rajapintaa
    // ja Tulostaja-luokkaa


    public static void main(String[] args){
    
        // GeneratorFIF:n toteutuksia:
    
       /* GeneratorFIF generatorOldWay = new GeneratorFIF(){
            public int get(){
                return 3;
            }
        }; */
        
        // Koska kyseessä funktionaalinen rajapinta, voidaan hyödyntää lambda-lausekkeita:
        
       IntSupplier i = () -> {
           return 3;
       };
        
        System.out.println(i.getAsInt());
        
        i = () -> 2;
        
        System.out.println(i.getAsInt());
        
        i = () -> (int) (Math.random() * 6 + 1);
        
        System.out.println(i.getAsInt());
       
    }   

    @Override
    public int getAsInt() {
        return 3;
    }

    
}