const Immutable = require('immutable');

const set1 = Immutable.Set(['punainen', 'vihreä', 'keltainen']);
const set2 = [ ...set1, 'ruskea']

console.log(set2)

set1 === set2;
console.log(set1)
console.log(set2)

//set2 === [ ...set1, 'ruskea']
//console.log(set2)

const set3 = [ ...set2, 'ruskea']

console.log(set2 === set3)