/*function toCelsius(fahrenheit) {
    return (5/9) * (fahrenheit-32);
}
function area(radius)   
    {  
        return Math.PI * radius * radius;  
    };  
*/ 

/* function area(radius)   
    {  
        return Math.PI * radius * radius;  
    };  
 */  
    
const toCelsius = (fahrenheit) => { 
    return (5/9) * (fahrenheit-32); 
}

const area = (radius) => {
    return Math.PI * radius * radius;
}